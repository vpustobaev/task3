package by.epam.java.task3.runner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import by.epam.java.task3.entity.Paragraph;
import by.epam.java.task3.entity.Sentence;
import by.epam.java.task3.entity.Word;
import by.epam.java.task3.util.Parser;

public class Main {

    public static void main(String[] args) {

	File file = new File("src/main/resources/text.txt");

	String content = null;
	try {
	    content = new Scanner(file).useDelimiter("\\Z").next();
	} catch (FileNotFoundException e) {

	    e.printStackTrace();
	}

	List<Paragraph> paragraphs = Parser.parseText(content);
	List<Sentence> sentences = Parser.parseParagraph(content);
	List<Word> words = Parser.parseSentence(content);

	System.out.println(words);

    }

}
