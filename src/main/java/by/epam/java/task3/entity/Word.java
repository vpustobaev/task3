package by.epam.java.task3.entity;

public class Word {

    private String value;

    public Word(String string) {
	this.value = string;

    }

    public String getValue() {
	return value;
    }

    public void setValue(String value) {
	this.value = value;
    }

    @Override
    public String toString() {
	return "value=" + value + "\n";
    }

}
