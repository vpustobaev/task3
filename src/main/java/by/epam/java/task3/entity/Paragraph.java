package by.epam.java.task3.entity;

import java.util.List;

public class Paragraph {

    private String textValue;

    private List<Sentence> sentences;

    public Paragraph(String text) {
	this.textValue = text;
    }

    public Paragraph() {

    }

    public String getValue() {
	return textValue;
    }

    public void setValue(String value) {
	this.textValue = value;
    }

    public List<Sentence> getSentences() {
	return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
	this.sentences = sentences;
    }

    @Override
    public String toString() {
	return "value=" + textValue + "\n";
    }

}
