package by.epam.java.task3.entity;

import java.util.List;

public class Text {

    private String value;

    private List<Paragraph> paragraphs;

    public Text(List<Paragraph> paragraphs) {
	this.paragraphs = paragraphs;
    }

    public Text() {

    }

    public List<Paragraph> getParagraphs() {
	return paragraphs;
    }

    public void setParagraphs(List<Paragraph> paragraphs) {
	this.paragraphs = paragraphs;
    }

    public String getValue() {
	return value;
    }

    public void setValue(String value) {
	this.value = value;
    }

    @Override
    public String toString() {
	return "paragraphs=" + paragraphs + "\n";
    }

}
