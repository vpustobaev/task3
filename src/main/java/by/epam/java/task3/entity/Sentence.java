package by.epam.java.task3.entity;

import java.util.List;

public class Sentence {

    private String value;

    private List<Word> words;

    public Sentence(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public void setValue(String value) {
	this.value = value;
    }

    public List<Word> getWords() {
	return words;
    }

    public void setWords(List<Word> words) {
	this.words = words;
    }

    @Override
    public String toString() {
	return "value=" + value + "\n";
    }

}