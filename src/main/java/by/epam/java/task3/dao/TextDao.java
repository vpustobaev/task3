package by.epam.java.task3.dao;

import java.util.ArrayList;

import by.epam.java.task3.exception.DaoException;

public interface TextDao {

    ArrayList<String> readInfo(String path) throws DaoException;

}
