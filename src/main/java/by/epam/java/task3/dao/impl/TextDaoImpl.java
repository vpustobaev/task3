package by.epam.java.task3.dao.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;

import by.epam.java.task3.dao.TextDao;
import by.epam.java.task3.exception.DaoException;

public class TextDaoImpl implements TextDao {

    private static final Logger logger = LogManager.getLogger(TextDaoImpl.class);
    private static final String FILE_NOT_FOUND = "File wasn't found, check the file path";
    private static final String PROBLEMS_WITH_FILE = "Is the file still accessible?";

    private ArrayList<String> result = new ArrayList<>();

    @Override
    public ArrayList<String> readInfo(String path) throws DaoException {

	BufferedReader br = null;
	try {
	    br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path))));
	} catch (FileNotFoundException e) {
	    logger.error(FILE_NOT_FOUND);
	    throw new DaoException(FILE_NOT_FOUND, e);
	}

	String line;

	try {
	    while ((line = br.readLine()) != null) {

		result.add(line);

	    }
	} catch (IOException e) {
	    logger.error(PROBLEMS_WITH_FILE);
	    throw new DaoException(PROBLEMS_WITH_FILE, e);
	}
	
	return result;

    }
}