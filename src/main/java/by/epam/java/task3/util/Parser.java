package by.epam.java.task3.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import by.epam.java.task3.entity.Paragraph;
import by.epam.java.task3.entity.Sentence;
import by.epam.java.task3.entity.Word;

public class Parser {

    private static final String TEXT_SPLITTER = "[^\\s][\\s\\S]*?[\\n\\r]";
    private static final String PARAGRAPH_SPLITTER = "[\\s\\S]*?([.\\!?][\\s]|[\\n\\r]|$)[\\s]?";
    private static final String WORD_SPLITTER = "([^\\s]+[a-zA-Z]*[\\.\\!?-�\\[]*[\\�]*(\\)\\;)*[\\]]?)";

    public static List<Paragraph> parseText(String textValue) {

	List<Paragraph> paragraphs = new ArrayList<>();

	Pattern paragraphPattern = Pattern.compile(TEXT_SPLITTER);
	Matcher paragraphMatcher = paragraphPattern.matcher(textValue);

	while (paragraphMatcher.find()) {

	    Paragraph paragraph = new Paragraph(paragraphMatcher.group());
	    paragraphs.add(paragraph);
	}

	return paragraphs;
    }

    public static List<Sentence> parseParagraph(String textValue) {

	List<Paragraph> paragraphs = parseText(textValue);

	List<Sentence> sentences = new ArrayList<>();

	Pattern sentencePattern = Pattern.compile(PARAGRAPH_SPLITTER);

	for (Paragraph pars : paragraphs) {
	    Matcher sentenceMatcher = sentencePattern.matcher(pars.getValue());
	    while (sentenceMatcher.find()) {
		Sentence sentence = new Sentence(sentenceMatcher.group());
		sentences.add(sentence);
	    }
	}

	return sentences;
    }

    public static List<Word> parseSentence(String textValue) {

	List<Sentence> sentences = parseParagraph(textValue);

	List<Word> words = new ArrayList<>();

	Pattern wordPattern = Pattern.compile(WORD_SPLITTER);

	for (Sentence sens : sentences) {
	    Matcher sentenceMatcher = wordPattern.matcher(sens.getValue());
	    while (sentenceMatcher.find()) {
		Word word = new Word(sentenceMatcher.group());
		words.add(word);
	    }
	}

	return words;
    }

}
